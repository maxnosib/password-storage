package main

import (
	"fmt"
	"net/http"
	"os"

	"gitlab.com/maxnosib/password-storage/api"
)

//go:generate go test ./... -coverprofile cp.out
//go:generate go tool cover -html=cp.out
func main() {
	CheckCreateFile("db")
	fs := http.FileServer(http.Dir("static"))
	http.Handle("/", fs)

	http.HandleFunc("/login", api.Login)
	http.HandleFunc("/add", api.AddData)

	fmt.Println("Server is listening...")
	http.ListenAndServe(":8181", nil)

}

func CheckCreateFile(name string) (*os.File, error) {
	if _, err := os.Stat(name); err != nil {
		return os.Create(name)
	}
	// TODO: подумать как сделать верную сигнатуру чтоб не было return nil, nil
	return nil, nil
}
