package violet

import (
	"errors"
	"testing"
)

type testDataU struct {
	key     []byte
	message string
	errMsg  error
}

var testCaseU = []testDataU{
	{key: []byte("abcdefghjklmnopqrstwxyz123456789"), message: "32", errMsg: nil},
	{key: []byte("1234567890abcdefghjklmno"), message: "24", errMsg: nil},
	{key: []byte("1234567890abcdef"), message: "16", errMsg: nil},
	{key: []byte("0"), message: "0", errMsg: errors.New("key length should be 16, 24, 32")},
}

func TestAverageU(t *testing.T) {
	for _, v := range testCaseU {
		er := checkKey(v.key)
		if er != nil && er.Error() != v.errMsg.Error() {
			t.Errorf("error: %v case: %s", er, v.message)
		}
	}
}
