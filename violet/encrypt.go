package violet

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"io"
	"io/ioutil"
)

// Encrypt encrypt string to base64 crypto using AES
func Encrypt(key []byte, cryptoText string) error {
	if err := checkKey(key); err != nil {
		return err
	}
	return encrypt(key, []byte(cryptoText))
}

func encrypt(key, text []byte) error {
	block, err := aes.NewCipher(key)
	if err != nil {
		return err
	}

	// The IV needs to be unique, but not secure. Therefore it's common to
	// include it at the beginning of the ciphertext.
	ciphertext := make([]byte, aes.BlockSize+len(text))
	iv := ciphertext[:aes.BlockSize]
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		return err
	}

	stream := cipher.NewCFBEncrypter(block, iv)
	stream.XORKeyStream(ciphertext[aes.BlockSize:], text)
	// convert to base64
	// base64.URLEncoding.EncodeToString(ciphertext)

	if err := ioutil.WriteFile("db", ciphertext, 0644); err != nil {
		return err
	}
	return nil
}
