package violet

import (
	"crypto/aes"
	"crypto/cipher"
	"errors"
	"fmt"
	"io/ioutil"
)

// Decrypt decrypt from base64 to decrypted string
func Decrypt(key []byte) (string, error) {
	if err := checkKey(key); err != nil {
		return "", err
	}
	return decrypt(key)
}

func decrypt(key []byte) (string, error) {
	// text base64 to []byte
	// ciphertext, _ := base64.URLEncoding.DecodeString(cryptoText)

	ciphertext, err := ioutil.ReadFile("db")

	block, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}

	// The IV needs to be unique, but not secure. Therefore it's common to
	// include it at the beginning of the ciphertext.
	if len(ciphertext) < aes.BlockSize {
		return "", errors.New("ciphertext too short")
	}
	iv := ciphertext[:aes.BlockSize]
	ciphertext = ciphertext[aes.BlockSize:]

	stream := cipher.NewCFBDecrypter(block, iv)

	// XORKeyStream can work in-place if the two arguments are the same.
	stream.XORKeyStream(ciphertext, ciphertext)

	return fmt.Sprintf("%s", ciphertext), nil
}
