package violet

import (
	"errors"
)

// check length key
func checkKey(key []byte) error {
	switch len(key) {
	default:
		return errors.New("key length should be 16, 24, 32")
	case 16, 24, 32:
		break
	}
	return nil
}
