package violet

import (
	"os"
	"testing"
)

type testDataED struct {
	key     []byte
	text    []byte
	message string
	errMsg  error
}

var testCaseED = []testDataED{
	{key: []byte("abcdefghjklmnopqrstwxyz123456789"), text: []byte("test text"), message: "1 case", errMsg: nil},
	{key: []byte("1234567890abcdefghjklmno"), text: []byte("test text"), message: "2 case", errMsg: nil},
	{key: []byte("1234567890abcdef"), text: []byte("test text"), message: "3 case", errMsg: nil},
}

func TestAverageED(t *testing.T) {
	for _, v := range testCaseED {
		er := encrypt(v.key, v.text)
		if er != nil {
			t.Errorf("error: %v case: %s", er, v.message)
		}
		text, er := decrypt(v.key)
		if er != nil {
			t.Errorf("error: %v case: %s", er, v.message)
		}

		if string(v.text) != text {
			t.Error("not equal text after decrypt")
		}
	}
	if err := os.Remove("db"); err != nil {
		t.Error("not remove db file")
	}
}
