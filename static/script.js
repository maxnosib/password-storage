function login(){
    let pass = document.getElementById('pass').value;

    let obj = {pass:pass};

    fetch('/login', {method: 'POST',body:JSON.stringify(obj),headers:{'content-type': 'application/json'}})
        .then(function (response) {
            return response.json();
        })
        .then(function (data) {
            // убираем форму логина
            document.getElementById('login-form').hidden = true;
            // выводим форму ответа
            document.getElementById('result').hidden = false;
            document.getElementById('add-data').hidden = false;

            viewResult(data);
        })
        .catch(console.log("error"));
}

function add(){
    let url = document.getElementById('url').value;
    let pwd = document.getElementById('pwd').value;

    let obj = {url:url,pass:pwd};

    fetch('/add', {method: 'POST',body:JSON.stringify(obj),headers:{'content-type': 'application/json'}})
        .then(function (response) {
            return response.json();
        })
        .then(function (data) {
            viewResult(data)
        })
        .catch(console.log("error"));
}

// выводим результат
function viewResult(data){
    let html = ''
    for(res in data){
        html += '<p><span class="url">'+data[res].url+'</span>: <span class="pwd">'+data[res].pass+'</span></p>'
    }
    document.getElementById('result').innerHTML = html
}
