package api

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strings"
)

func parseResponse(r *http.Request, in interface{}) error {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return err
	}
	return json.Unmarshal(body, in)
}

func stringToResult(in string) []result {
	var res []result
	arrStr := strings.Split(in[:len(in)-1], ",")
	for _, v := range arrStr {
		txt := strings.Split(v, ":")
		if len(txt) != 2 {
			continue
		}
		r := result{URL: txt[0], Pwd: txt[1]}
		res = append(res, r)
	}
	return res
}
