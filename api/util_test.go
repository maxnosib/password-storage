package api

import (
	"bytes"
	"net/http"
	"testing"
)

type answerParseResponseS struct {
	Name string
}

type parseResponseS struct {
	body    []byte
	answer  answerParseResponseS
	message string
	errMsg  error
}

var testCaseParseResponse = []parseResponseS{
	{body: []byte(`{"name":"test message"}`), answer: answerParseResponseS{"test message"}, message: "1"},
}

func TestParseResponse(t *testing.T) {
	for _, v := range testCaseParseResponse {
		req, err := http.NewRequest(http.MethodPost, "test", bytes.NewReader(v.body))
		if err != nil {
			t.Errorf("error: %v case: %s", err, v.message)
		}

		var result answerParseResponseS
		er := parseResponse(req, &result)
		if er != nil {
			t.Errorf("error: %v case: %s", er, v.message)
		}

		if result.Name != v.answer.Name {
			t.Errorf("error: %v case: %s", "answer not equal", v.message)
		}
	}
}

type stringToResultS struct {
	in      string
	res     []result
	message string
	errMsg  error
}

var testStringToResult = []stringToResultS{
	{in: "url1:pwd1,url2:pwd2,", res: []result{result{"url1", "pwd1"}, result{"url2", "pwd2"}}, message: "1"},
}

func TestStringToResult(t *testing.T) {
	for _, val := range testStringToResult {
		res := stringToResult(val.in)
		for k, v := range val.res {
			if v.URL != res[k].URL || v.Pwd != res[k].Pwd {
				t.Errorf("error: %v case: %s", "answer not equal", val.message)
			}
		}
	}
}
