package api

import (
	"crypto/md5"
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/maxnosib/password-storage/violet"
)

var key []byte

// Login get data + set key
func Login(w http.ResponseWriter, r *http.Request) {
	var in login
	err := parseResponse(r, &in)
	if err != nil {
		http.Error(w, "Error read request", http.StatusInternalServerError)
	}

	hash := md5.Sum([]byte(key))
	key = []byte(fmt.Sprintf("%x", hash))

	text, err := violet.Decrypt(key)
	if err != nil {
		http.Error(w, "Error decrypt", http.StatusInternalServerError)
	}

	res := stringToResult(text)

	jsonBody, err := json.Marshal(res)
	if err != nil {
		http.Error(w, "Error converting results to json", http.StatusInternalServerError)
	}
	w.Write(jsonBody)
}

// AddData add new rows
func AddData(w http.ResponseWriter, r *http.Request) {
	var in *result
	err := parseResponse(r, &in)
	if err != nil {
		http.Error(w, "Error read request", http.StatusInternalServerError)
	}

	text, err := violet.Decrypt(key)
	if err != nil {
		http.Error(w, "Error decrypt", http.StatusInternalServerError)
	}

	text += fmt.Sprintf("%s:%s,", in.URL, in.Pwd)

	err = violet.Encrypt(key, text)
	if err != nil {
		http.Error(w, "Error encrypt", http.StatusInternalServerError)
	}

	res := stringToResult(text)

	jsonBody, err := json.Marshal(res)
	if err != nil {
		http.Error(w, "Error converting results to json", http.StatusInternalServerError)
	}
	w.Write(jsonBody)
}
